﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BasketballTeams.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace BasketballTeams.Context
{
    public class BasketballDbContext : DbContext
    {
        public DbSet<Team> Teams { get; set; }
        DbSet<Person> People { get; set; }
        DbSet<Coach> Coaches { get; set; }
        DbSet<Player> Players { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(@"C:\Users\melma\source\repos\basketballteams\BasketballTeams\BasketballTeams");
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            var connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Team>()
        //        .HasIndex(u => u.Name)
        //        .IsUnique();
        //}
    }
}
