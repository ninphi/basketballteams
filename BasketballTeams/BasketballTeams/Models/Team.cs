﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BasketballTeams.Enums;
using BasketballTeams.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BasketballTeams.Models
{
    public class Team : IEntity<int>
    {
        public int Id { get; set; }

        [Required]
        [ForeignKey("Coach")]
        public int CoachId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public Conference Conference { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        [Required]
        public int WinCount { get; set; }

        [Required]
        public int LoseCount { get; set; }

        public Coach Coach { get; set; }
        public List<Player> Players { get; set; }
    }
}
