﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasketballTeams.Models
{
    public class Coach : Person
    {
        [Required]
        public int YearsOfWork { get; set; }
    }
}
