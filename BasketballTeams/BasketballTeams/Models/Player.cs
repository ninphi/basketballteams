﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BasketballTeams.Enums;

namespace BasketballTeams.Models
{
    public class Player : Person
    {
        [Required]
        [ForeignKey("Team")]
        public int TeamId { get; set; }

        [Required]
        public float Height { get; set; }

        [Required]
        public float Weight { get; set; }

        [Required]
        public int JerseyNumber { get; set; }

        [Required]
        public PlayingPosition Position { get; set; }

        public Team Team { get; set; }
    }
}
